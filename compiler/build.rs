extern crate lalrpop;

use std::{env, fs};
use std::path::Path;

fn main() {
	lalrpop::process_root().unwrap();
	
	// This is needed to fix a bug with lalrpop
	let out_dir = env::var("OUT_DIR").unwrap();
	let out_dir = Path::new(&out_dir);
	fs::copy(out_dir.join("compiler/parser/grammar.rs"), out_dir.join("grammar.rs")).unwrap();
	
	// This is needed as rustacuda uses a non-standard environment variable to find the CUDA install location
	println!("cargo:rustc-env=LIBRARY_PATH={}", env::var("CUDA_LIBRARY_PATH").unwrap());
}
