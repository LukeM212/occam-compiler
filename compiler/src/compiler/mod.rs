mod lexer;
use lexer::lex;

mod parser;
use parser::parse;

mod instr_gen;
use instr_gen::generate;

mod peephole;
use peephole::peephole_optimise;

mod assembler;
use assembler::assemble;

use crate::AnyError;

pub fn compile(occam: String) -> Result<Vec<u8>, AnyError> {
	let token_stream = lex(&occam)?;
	
	let syntax_tree = parse(token_stream)?;
	
// BEGIN peephole
	let transputer_assembly = generate(syntax_tree)?;
	
	let optimised_assembly = peephole_optimise(transputer_assembly)?;
	
	let transputer_code = assemble(optimised_assembly)?;
// END peephole
	
	Ok(transputer_code)
}
