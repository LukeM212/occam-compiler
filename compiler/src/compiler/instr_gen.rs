use std::rc::Rc;
use std::cmp::max;
use std::fmt::Debug;
use crate::instruction_set::{Encoding, OpCode, Operation};
use crate::common::List;
use crate::AnyError;
use super::parser::ast::{Occam, Proc, Expr, ValType, MonOp, BinOp, ArraySize,
	ArrayDims, ElemType};
use crate::{list, listp};
use simple_error::bail;

pub fn generate(program: Occam) -> Result<Asm, AnyError> {
	let mut label_counter = Counter::new();
	
	let (asm, stack) =
		translate_stmt(program.program, Environment::new(), &mut label_counter)?;
	
	// println!("Stack required: {}", stack);
	
	Ok(
		list![
			..asm,
			Op::StopP,
		]
	)
}

type Label = (u32, i32);

#[derive(Clone)]
pub enum Environment {
	Nil,
	Item(Rc<(String, EnvItem, Environment)>),
	StaticLink(Rc<Environment>),
}

impl Environment {
	pub fn new() -> Self {
		Environment::Nil
	}
	
	pub fn extend_proc(&self, name: String, label: Label, stack: u32)
		-> Environment {
		Environment::Item(Rc::new((name,
			EnvItem::Proc(label, stack),
			self.clone())))
	}
	
	pub fn extend_var(&self, name: String, var: Type) -> Environment {
		Environment::Item(Rc::new((name,
			EnvItem::Var(var),
			self.clone())))
	}
	
	pub fn extend_def(&self, name: String, value: Expr) -> Environment {
		Environment::Item(Rc::new((name,
			EnvItem::Def(value),
			self.clone())))
	}
	
	pub fn extend_static_link(&self) -> Environment {
		Environment::StaticLink(Rc::new(self.clone()))
	}
	
	pub fn lookup(&self, name: String) -> Option<(List<i32>, EnvItem)> {
		match self {
			Environment::Nil => None,
			Environment::Item(rc) => {
				let (ref item_name, ref item_type, ref rest) = **rc;
				if *item_name == name {
					Some((list![0], (*item_type).clone()))
				}
				else if let Some((listp![offset, ..offsets], inner_type)) =
					rest.lookup(name) {
					let new_offset = offset + match item_type {
						EnvItem::Var(var_type) => size_of(var_type.clone()) as i32,
						_ => 0,
					};
					Some((list![new_offset, ..offsets], inner_type))
				}
				else { None }
			},
			Environment::StaticLink(ref rest) => {
				if let Some((offsets, inner_type)) = rest.lookup(name) {
					Some((list![0, ..offsets], inner_type))
				}
				else { None }
			},
		}
	}
}

#[derive(Clone)]
pub enum EnvItem {
	Proc(Label, u32),
	Var(Type),
	Def(Expr),
}

#[derive(Clone)]
pub enum Type {
	Word,
	Chan,
	Array(u32, Box<Type>),
	ArrayRef(Box<Type>),
	//Closure?,
}

struct Counter {
	current: u32,
}

impl Counter {
	pub fn new() -> Self {
		Self { current: 0 }
	}
	
	pub fn next(&mut self) -> (u32, i32) {
		self.current += 1;
		(self.current, 0)
	}
}

pub type Asm = List<Op>;

#[derive(Copy, Clone, Debug)]
pub enum Op {
	Label(Label),
	J(Label),
	CJ(Label),
	
	AjW(i32),
	StartP(Label),
	EndP,
	StopP,
	RunP,
	Rev,
	Copy,
	Ret,
	
	NewChan,
	In,
	Out,
	
	LdC(i32),
	LdL(i32),
	LdLP(i32),
	LdPI(Label),
	LdNL(i32),
	LdNLP(i32),
	StL(i32),
	StNL(i32),
	
	Swap,
	AddC(i32),
	EqC(i32),
	Not,
	Add,
	Sub,
	Mul,
	Div,
	Rem,
	And,
	Or,
	XOr,
	ShL,
	ShR,
	GT,
}

impl Op {
	pub fn encode(self) -> List<Encoding> {
		match self {
			Op::Label(_) => list![],
			Op::J(label) => OpCode::J.encode(label.1),
			Op::CJ(label) => OpCode::CJ.encode(label.1),
			//Op::Call(label) => OpCode::Call.encode(label.1),
			
			Op::AjW(offset) => OpCode::AjW.encode(offset),
			Op::StartP(label) => list![
				..OpCode::LdC.encode(label.1),
				..Operation::StartP.encode(),
			],
			Op::EndP => Operation::EndP.encode(),
			Op::StopP => Operation::StopP.encode(),
			Op::RunP => Operation::RunP.encode(),
			Op::Rev => Operation::Rev.encode(),
			Op::Copy => Operation::Copy.encode(),
			Op::Ret => Operation::Ret.encode(),
			
			Op::NewChan => OpCode::LdC.encode(-1),
			Op::In => Operation::In.encode(),
			Op::Out => Operation::Out.encode(),
			
			Op::LdC(x) => OpCode::LdC.encode(x),
			Op::LdL(offset) => OpCode::LdL.encode(offset),
			Op::LdLP(offset) => OpCode::LdLP.encode(offset),
			Op::LdPI(label) => list![
				..OpCode::LdC.encode(label.1),
				..Operation::LdPI.encode(),
			],
			Op::LdNL(offset) => OpCode::LdNL.encode(offset),
			Op::LdNLP(offset) => OpCode::AdC.encode(offset),
			Op::StL(offset) => OpCode::StL.encode(offset),
			Op::StNL(offset) => OpCode::StNL.encode(offset),
			
			Op::Swap => Operation::Rev.encode(),
			Op::AddC(x) => OpCode::AdC.encode(x),
			Op::EqC(x) => OpCode::EqC.encode(x),
			Op::Not => Operation::Not.encode(),
			Op::Add => Operation::Add.encode(),
			Op::Sub => Operation::Sub.encode(),
			Op::Mul => Operation::Mul.encode(),
			Op::Div => Operation::Div.encode(),
			Op::Rem => Operation::Rem.encode(),
			Op::And => Operation::And.encode(),
			Op::Or => Operation::Or.encode(),
			Op::XOr => Operation::XOr.encode(),
			Op::ShL => Operation::ShL.encode(),
			Op::ShR => Operation::ShR.encode(),
			//Op::WSub => Operation::WSub.encode(),
			Op::GT => Operation::GT.encode(),
		}
	}
}

fn translate_stmt(
	stmt: Proc, env: Environment,
	label_counter: &mut Counter
) -> Result<(Asm, u32), AnyError> {
	Ok(match stmt {
		Proc::Skip => (list![], 0),
		Proc::Stop => (list![Op::StopP], size_of(Type::Word)),
		Proc::Val(val_type, name, array_size, box rest) => {
			match val_type {
				ValType::Def(expr) => {
					let new_env = env.extend_def(name, expr);
					translate_stmt(rest, new_env, label_counter)?
				},
				t => {
					let var_type = translate_def_type(match t {
						ValType::Var => Type::Word,
						ValType::Chan => Type::Chan,
						_ => bail!("Unsupported feature"),
					}, array_size);
					let var_size = size_of(var_type.clone());
					let (init, init_stack) =
						translate_def(var_type.clone(), label_counter);
					let (asm, stack) = translate_stmt(rest,
						env.extend_var(name, var_type.clone()), label_counter)?;
					(
						list![
							Op::AjW(-(var_size as i32)),
							Op::LdLP(0),
							..init,
							..asm,
							Op::AjW(var_size as i32),
						],
						var_size + max(stack, init_stack)
					)
				},
			}
		},
		Proc::Proc(name, args, box body, box rest) => {
			let proc_label = label_counter.next();
			let end_label = label_counter.next();
			let mut proc_env = env.extend_static_link();
			for (arg_type, arg_name, arg_array_size) in args {
				let arg_type = translate_arg_type(match arg_type {
					ValType::Var | ValType::Value => Type::Word,
					ValType::Chan => Type::Chan,
					_ => bail!("Unsupported feature"),
				}, arg_array_size);
				proc_env = proc_env.extend_var(arg_name, arg_type);
			}
			proc_env = proc_env.extend_var("<RETURN_ADDR>".into(), Type::Word);
			let (proc_asm, proc_stack) = translate_stmt(body, proc_env, label_counter)?;
			let new_env = env.extend_proc(name, proc_label, proc_stack);
			let (rest_asm, rest_stack) = translate_stmt(rest, new_env, label_counter)?;
			(
				list![
					Op::J(end_label),
					Op::Label(proc_label),
					..proc_asm,
					Op::Ret,
					Op::Label(end_label),
					..rest_asm,
				],
				rest_stack + size_of(Type::Word)
			)
		},
		Proc::Assign(lhs, rhs) => {
			let (asm, stack) = translate_2(Expr::MonOp(MonOp::Addr, box lhs), rhs,
				env.clone(), label_counter)?;
			(
				list![
					..asm,
					Op::StNL(0),
				],
				if stack > 3 { (stack - 3) * size_of(Type::Word) } else { 0 }
			)
		},
		Proc::Send(chan, vals) => {
			let mut asm = list![];
			let mut new_env = env;
			let mut extra_stack = 0;
			let mut max_stack = 0;
			for val in vals.clone().reverse() {
				let (val_asm, val_stack) =
					translate_expr(val.clone(), new_env.clone(), label_counter)?;
				new_env = new_env.extend_var("<VAL>".into(), Type::Word);
				asm.push(list![
					..val_asm,
					Op::AjW(-(size_of(Type::Word) as i32)),
					Op::StL(0),
				]);
				max_stack = max(max_stack, if val_stack > 3 {
					(val_stack - 3) * size_of(Type::Word) } else { 0 } + extra_stack);
				extra_stack += size_of(Type::Word);
			}
			let (args_asm, args_stack) = translate_3(
				Expr::Const(extra_stack as i32),
				Expr::MonOp(MonOp::Addr, box chan.clone()),
				Expr::MonOp(MonOp::Addr, box Expr::Name("<VAL>".into())),
				new_env, label_counter
			)?;
			max_stack = max(max_stack, if args_stack > 3 {
					(args_stack - 3) * size_of(Type::Word) } else { 0 } + extra_stack);
			(
				list![
					..asm.reverse().flatten(),
					..args_asm,
					Op::Out,
					Op::AjW(extra_stack as i32),
				],
				max(max_stack, size_of(Type::Word) * 2 + extra_stack)
			)
		},
		Proc::Receive(chan, vars) => {
			let mut asm = list![];
			let mut new_env = env.clone();
			let mut extra_stack = 0;
			let mut max_stack = 0;
			for var in vars.clone().reverse() {
				new_env = new_env.extend_var("<VAL>".into(), Type::Word);
				let (val_asm, val_stack) = translate_stmt(Proc::Assign(var,
					Expr::Name("<VAL>".into())), new_env.clone(), label_counter)?;
				asm.push(list![
					..val_asm,
					Op::AjW(size_of(Type::Word) as i32),
				]);
				extra_stack += size_of(Type::Word);
				max_stack = max(max_stack, val_stack + extra_stack);
			}
			let (args_asm, args_stack) = translate_3(
				Expr::Const(extra_stack as i32),
				Expr::MonOp(MonOp::Addr, box chan.clone()),
				Expr::MonOp(MonOp::Addr, box Expr::Name("<VAL>".into())),
				new_env, label_counter
			)?;
			max_stack = max(max_stack, if args_stack > 3 {
				(args_stack - 3) * size_of(Type::Word) } else { 0 } + extra_stack);
			(
				list![
					Op::AjW(-(extra_stack as i32)),
					..args_asm,
					Op::In,
					..asm.flatten(),
				],
				max(max_stack, size_of(Type::Word) * 2 + extra_stack)
			)
		},
		Proc::Call(Expr::Name(name), args) => {
			match env.lookup(name).ok_or("Use of undefined variable")? {
				(chain, EnvItem::Proc(label, stack)) => {
					let mut chain = chain.reverse();
					let last = chain.pop().unwrap();
					let chain = chain.reverse();
					let mut asm = list![];
					asm.push(list![
						Op::LdLP(0),
						..chain.map(|o| Op::LdNL(o)).collect::<Asm>(),
						Op::LdNLP(last),
						Op::AjW(-(size_of(Type::Word) as i32)),
						Op::StL(0),
					]);
					let mut new_env = env.extend_var("<STATIC_LINK>".into(), Type::Word);
					let mut extra_stack = size_of(Type::Word);
					let mut max_stack = size_of(Type::Word);
					for arg in args.clone() {
						let (arg_asm, arg_stack) = translate_expr(arg.clone(),
							new_env.clone(), label_counter)?;
						new_env = new_env.extend_var("<ARG>".into(), Type::Word);
						asm.push(list![
							..arg_asm,
							Op::AjW(-(size_of(Type::Word) as i32)),
							Op::StL(0),
						]);
						max_stack = max(max_stack, if arg_stack > 3 {
							(arg_stack - 3) * size_of(Type::Word) } else { 0 } + extra_stack);
						extra_stack += size_of(Type::Word);
					}
					let ret_label = label_counter.next();
					(
						list![
							..asm.reverse().flatten(),
							Op::AjW(size_of(Type::Word) as i32),
							Op::LdPI(ret_label),
							Op::StL(0),
							Op::Label(ret_label),
							Op::AjW(extra_stack as i32),
						],
						max(max_stack, stack + extra_stack)
					)
				},
				_ => bail!("Cannot call variable or constant"),
			}
		},
		Proc::If(branches) => {
			let end_label = label_counter.next();
			let mut bodies = list![];
			let mut max_stack = 0;
			for (condition, body) in branches {
				let branch_end = label_counter.next();
				let (cond, cond_stack) =
					translate_expr(condition, env.clone(), label_counter)?;
				max_stack = max(max_stack, if cond_stack > 3 {
					(cond_stack - 3) * size_of(Type::Word) } else { 0 });
				let (body, body_stack) =
					translate_stmt(body, env.clone(), label_counter)?;
				max_stack = max(max_stack, body_stack);
				bodies.push(list![
					..cond,
					Op::CJ(branch_end),
					..body,
					Op::Label(branch_end),
				]);
			}
			bodies.push(list![
				Op::Label(end_label),
			]);
			(
				bodies.reverse().flatten(),
				max_stack
			)
		},
		Proc::Par(branches) => {
			let mut asm = list![];
			let mut bodies = list![];
			let mut stack = size_of(Type::Word);
			let parent_env = env.extend_var("<COUNTER>".into(), Type::Word);
			for branch in branches.clone() {
				let new_env = parent_env.extend_static_link();
				let (branch_asm, branch_stack) =
					translate_stmt(branch, new_env, label_counter)?;
				let branch_label = label_counter.next();
				stack += size_of(Type::Word);
				asm.push(list![
					Op::LdLP(0),
					Op::StL(-(stack as i32)),
					Op::LdLP(-(stack as i32)),
					Op::StartP(branch_label),
				]);
				bodies.push(list![
					Op::Label(branch_label),
					..branch_asm,
					Op::LdL(0),
					Op::EndP,
				]);
				stack += branch_stack;
			}
			let end_label = label_counter.next();
			(
				list![
					Op::AjW(-(size_of(Type::Word) as i32)),
					Op::LdC(branches.len()),
					Op::StL(0),
					..asm.reverse().flatten(),
					Op::LdPI(end_label),
					Op::StL(-(size_of(Type::Word) as i32)),
					Op::LdLP(0),
					Op::EndP,
					..bodies.reverse().flatten(),
					Op::Label(end_label),
					Op::AjW(size_of(Type::Word) as i32),
				],
				stack + size_of(Type::Word)
			)
		},
		Proc::Seq(stmts) => {
			let mut max_stack = 0;
			(
				stmts
					.map(|s| {
						let (asm, stack) = translate_stmt(s, env.clone(), label_counter)?;
						max_stack = max(max_stack, stack);
						Ok(asm)
					})
					.collect::<Result<List<Asm>, AnyError>>()?
					.flatten(),
				max_stack
			)
		},
		//Proc::Alt(branches) => {
			// List<(Option<Expr>, Option<(Expr, Name)>, Proc)>
		//},
// BEGIN translate_while
		Proc::While(condition, box body) => {
			let (cond_asm, cond_stack) =
				translate_expr(condition, env.clone(), label_counter)?;
			let (body_asm, body_stack) =
				translate_stmt(body, env.clone(), label_counter)?;
			let max_stack = max(body_stack, if cond_stack > 3 {
				(cond_stack - 3) * size_of(Type::Word)
			} else { 0 });
			let start_label = label_counter.next();
			let end_label = label_counter.next();
			(
				list![
					Op::Label(start_label),
					..cond_asm,
					Op::CJ(end_label),
					..body_asm,
					Op::J(start_label),
					Op::Label(end_label),
				],
				max_stack,
			)
		},
// END translate_while
		Proc::SeqLoop(name, start, count, box body) => {
			let mut max_stack = 0;
			let (start_asm, start_stack) =
				translate_expr(start, env.clone(), label_counter)?;
			max_stack = max(max_stack, if start_stack > 3 {
				(start_stack - 3) * size_of(Type::Word) } else { 0 });
			// Create two environments, one with the counter hidden, one with it visible
			// This is so the counter is not in scope while evaluating count
			let hidden_env = env.extend_var("<COUNTER>".into(), Type::Word);
			let visible_env = env.extend_var(name.clone(), Type::Word);
			let (end_asm, end_stack) = translate_expr(
				Expr::BinOp(BinOp::Add, box Expr::Name("<COUNTER>".into()), box count),
				hidden_env.clone(), label_counter)?;
			max_stack = max(max_stack, if end_stack > 3 {
				(end_stack - 3) * size_of(Type::Word) } else { 0 } + size_of(Type::Word));
			let body_env = visible_env.extend_var("<END>".into(), Type::Word);
			let (while_asm, while_stack) = translate_stmt(
				Proc::While(
					Expr::BinOp(BinOp::LT,
						box Expr::Name(name.clone()), box Expr::Name("<END>".into())),
					box Proc::Seq(list![
						body,
						Proc::Assign(Expr::Name(name.clone()),
							Expr::BinOp(BinOp::Add,
								box Expr::Name(name.clone()),
								box Expr::Const(1))),
					]),
				),
				body_env,
				label_counter,
			)?;
			max_stack = max(max_stack, if while_stack > 3 {
				(while_stack - 3) * size_of(Type::Word)
			} else { 0 } + 2 * size_of(Type::Word));
			(
				list![
					..start_asm,
					Op::AjW(-(size_of(Type::Word) as i32)),
					Op::StL(0),
					..end_asm,
					Op::AjW(-(size_of(Type::Word) as i32)),
					Op::StL(0),
					..while_asm,
					Op::AjW(size_of(Type::Word) as i32 * 2),
				],
				max_stack,
			)
		},
		Proc::ParLoop(name, start, Expr::Const(count), box body) => {
			let parent_env = env
				.extend_var("<CURRENT>".into(), Type::Word)
				.extend_var("<COUNTER>".into(), Type::Word);
			let new_env = parent_env
				.extend_static_link()
				.extend_var(name.clone(), Type::Word);
			let (body_asm, body_stack) = translate_stmt(body, new_env, label_counter)?;
			let body_label = label_counter.next();
			let loop_label = label_counter.next();
			let wait_label = label_counter.next();
			let end_label = label_counter.next();
			(
				list![
					Op::AjW(-(size_of(Type::Word) as i32 * 2)),
					Op::LdC(0),
					Op::StL(size_of(Type::Word) as i32),
					Op::LdC(count),
					Op::StL(0),
					Op::Label(loop_label),
					Op::LdC(count),
					Op::LdL(size_of(Type::Word) as i32),
					Op::GT,
					Op::CJ(wait_label),
					Op::LdL(size_of(Type::Word) as i32),
					Op::LdC(-(body_stack as i32 + 2 * (size_of(Type::Word) as i32))),
					Op::Mul,
					Op::LdLP(-2 * (size_of(Type::Word) as i32)),
					Op::Add,
					Op::Copy,
					Op::LdLP(0),
					Op::Swap,
					Op::StNL(0),
					Op::Copy,
					Op::LdL(size_of(Type::Word) as i32),
					Op::Swap,
					Op::StNL(-(size_of(Type::Word) as i32)),
					Op::StartP(body_label),
					Op::LdL(size_of(Type::Word) as i32),
					Op::AddC(1),
					Op::StL(size_of(Type::Word) as i32),
					Op::J(loop_label),
					Op::Label(body_label),
					Op::AjW(-(size_of(Type::Word) as i32)),
					..body_asm,
					Op::AjW(size_of(Type::Word) as i32),
					Op::LdL(0),
					Op::EndP,
					Op::Label(wait_label),
					Op::LdPI(end_label),
					Op::StL(-(size_of(Type::Word) as i32)),
					Op::LdLP(0),
					Op::EndP,
					Op::Label(end_label),
					Op::AjW(size_of(Type::Word) as i32 * 2),
				],
				(body_stack + 2 * size_of(Type::Word)) * (count as u32)
					+ 4 * size_of(Type::Word)
			)
		},
		//Proc::IfLoop(Name, Expr, Expr, Expr, Box<Proc>),
		_ => bail!("Unsupported feature"),
	})
}

fn translate_expr(
	expr: Expr, env: Environment,
	label_counter: &mut Counter
) -> Result<(Asm, u32), AnyError> {
	Ok(match expr {
		Expr::Const(c) => (list![Op::LdC(c)], 1),
		Expr::Name(ref name) => {
			match env.lookup(name.clone()).ok_or("Use of undefined variable")? {
				(chain, EnvItem::Var(var_type)) =>
					if is_ref_type(var_type) {
						translate_addr(expr, env, label_counter)?
					} else { (
						list![
							Op::LdLP(0),
							..chain.map(|o| Op::LdNL(o)).collect::<Asm>(),
						],
						1
					) },
				// Note that this means that def is evaluated in the current scope, this
				// may not be what is wanted but this is simplest to implement
				(_, EnvItem::Def(def)) => translate_expr(def, env, label_counter)?,
				_ => bail!("Does not support first class functions"),
			}
		},
		// Expr::Table(),
		Expr::Index(index_type, box array, box index) => {
			match index_type {
				ElemType::Word => translate_expr(Expr::MonOp(MonOp::Load,
					box Expr::BinOp(BinOp::Add, box Expr::MonOp(MonOp::Addr, box array),
					box Expr::BinOp(BinOp::Mul, box index,
					box Expr::Const(size_of(Type::Word) as i32)))), env, label_counter)?,
				ElemType::Byte => translate_expr(Expr::MonOp(MonOp::Load,
					box Expr::BinOp(BinOp::Add, box Expr::MonOp(MonOp::Addr, box array),
					box index)), env, label_counter)?,
			}
		},
		Expr::MonOp(op, box expr) => {
			match op {
				MonOp::Addr => translate_addr(expr, env, label_counter)?,
				MonOp::Load => {
					let (expr, expr_stack) = translate_expr(expr, env, label_counter)?;
					(
						list![
							..expr,
							Op::LdNL(0),
						],
						expr_stack
					)
				},
				MonOp::Neg => translate_expr(Expr::BinOp(BinOp::Sub, box Expr::Const(0),
					box expr), env, label_counter)?,
				MonOp::Not => {
					let (expr, expr_stack) = translate_expr(expr, env, label_counter)?;
					(
						list![
							..expr,
							Op::Not,
						],
						expr_stack
					)
				},
			}
		},
// BEGIN translate_binop
		Expr::BinOp(op, box lhs, box rhs) => {
			fn args_then(a_reg: Expr, b_reg: Expr, then: Asm, env: Environment,
				label_counter: &mut Counter) -> Result<(Asm, u32), AnyError> {
				let (args, args_stack) = translate_2(a_reg, b_reg, env, label_counter)?;
				Ok((
					list![
						..args,
						..then,
					],
					args_stack
				))
			}
			
			match op {
				BinOp::Add => args_then(rhs, lhs, list![Op::Add], env, label_counter)?,
				BinOp::Sub => args_then(rhs, lhs, list![Op::Sub], env, label_counter)?,
				BinOp::Mul => args_then(rhs, lhs, list![Op::Mul], env, label_counter)?,
				BinOp::Div => args_then(rhs, lhs, list![Op::Div], env, label_counter)?,
				BinOp::Mod => args_then(rhs, lhs, list![Op::Rem], env, label_counter)?,
				BinOp::LSL => args_then(rhs, lhs, list![Op::ShL], env, label_counter)?,
				BinOp::LSR => args_then(rhs, lhs, list![Op::ShR], env, label_counter)?,
				BinOp::And => args_then(rhs, lhs, list![Op::And], env, label_counter)?,
				BinOp::Or => args_then(rhs, lhs, list![Op::Or], env, label_counter)?,
				BinOp::XOr => args_then(rhs, lhs, list![Op::XOr], env, label_counter)?,
				BinOp::Eq => args_then(rhs, lhs, list![Op::XOr, Op::EqC(0)], env,
					label_counter)?,
				BinOp::NEq => args_then(rhs, lhs, list![Op::XOr, Op::EqC(0), Op::Not], env,
					label_counter)?,
				BinOp::LT => args_then(lhs, rhs, list![Op::GT], env, label_counter)?,
				BinOp::GT => args_then(rhs, lhs, list![Op::GT], env, label_counter)?,
				BinOp::LEq => args_then(rhs, lhs, list![Op::GT, Op::Not], env,
					label_counter)?,
				BinOp::GEq => args_then(lhs, rhs, list![Op::GT, Op::Not], env,
					label_counter)?,
				_ => bail!("Unsupported feature"),
			}
		},
// END translate_binop
		//Expr::Call(Box<Expr>, List<Expr>),
		_ => bail!("Unsupported feature"),
	})
}

fn translate_addr(
	expr: Expr, env: Environment,
	label_counter: &mut Counter
) -> Result<(Asm, u32), AnyError> {
	Ok(match expr {
		Expr::Const(_) => bail!("Cannot find the address of a literal"),
		Expr::Name(name) => {
			match env.lookup(name).ok_or("Use of undefined variable")? {
				(chain, EnvItem::Var(_)) => {
					let mut chain = chain.reverse();
					let last = chain.pop().unwrap();
					let chain = chain.reverse();
					(
						list![
							Op::LdLP(0),
							..chain.map(|o| Op::LdNL(o)).collect::<Asm>(),
							Op::LdNLP(last),
						],
						1
					)
				},
				(_, EnvItem::Def(_)) => bail!("Cannot find the address of a constant"),
				_ => bail!("Does not support first class functions"),
			}
		},
		// Expr::Table(),
		Expr::Index(index_type, box array, box index) => {
			match index_type {
				ElemType::Word => translate_expr(Expr::BinOp(BinOp::Add, box Expr::MonOp(
					MonOp::Addr, box array), box Expr::BinOp(BinOp::Mul, box index,
					box Expr::Const(size_of(Type::Word) as i32))), env, label_counter)?,
				ElemType::Byte => translate_expr(Expr::BinOp(BinOp::Add, box Expr::MonOp(
					MonOp::Addr, box array), box index), env, label_counter)?,
			}
		},
		Expr::MonOp(_, _) => bail!("Cannot find the address of an expression"),
		Expr::BinOp(_, _, _) => bail!("Cannot find the address of an expression"),
		//Expr::Call(Box<Expr>, List<Expr>),
		_ => bail!("Unsupported feature"),
	})
}

fn translate_def(var_type: Type, label_counter: &mut Counter) -> (Asm, u32) {
	match var_type {
		Type::Word | Type::ArrayRef(_) => (
			list![
				Op::LdNLP(size_of(Type::Word) as i32),
			],
			0
		),
		Type::Chan => (
			list![
				Op::Copy,
				Op::NewChan,
				Op::Swap,
				Op::StNL(0),
				Op::LdNLP(size_of(Type::Word) as i32),
			],
			0
		),
		Type::Array(n, box inner_type) => {
			let start_label = label_counter.next();
			let end_label = label_counter.next();
			let (inner_asm, inner_stack) =
				translate_def(inner_type.clone(), label_counter);
			if is_ref_type(inner_type.clone()) { (
				list![
					Op::AjW(-2 * (size_of(Type::Word) as i32)),
					Op::Copy,
					Op::StL(0),
					Op::LdNLP((n as i32) * (size_of(Type::Word) as i32)),
					Op::Copy,
					Op::StL(size_of(Type::Word) as i32),
					Op::Label(start_label),
					Op::LdL(0),
					Op::LdL(size_of(Type::Word) as i32),
					Op::XOr,
					Op::CJ(end_label),
					Op::Copy,
					Op::LdL(0),
					Op::StNL(0),
					..inner_asm,
					Op::LdL(0),
					Op::LdNLP(size_of(Type::Word) as i32),
					Op::StL(0),
					Op::J(start_label),
					Op::Label(end_label),
					Op::AjW(2 * (size_of(Type::Word) as i32)),
				],
				inner_stack + 2 * size_of(Type::Word)
			) } else { (
				list![
					Op::AjW(-(size_of(Type::Word) as i32)),
					Op::Copy,
					Op::LdNLP((n as i32) * (size_of(Type::Word) as i32)),
					Op::StL(0),
					Op::Label(start_label),
					Op::Copy,
					Op::LdL(0),
					Op::XOr,
					Op::CJ(end_label),
					..inner_asm,
					Op::J(start_label),
					Op::Label(end_label),
					Op::AjW(size_of(Type::Word) as i32),
				],
				inner_stack + size_of(Type::Word)
			) }
		},
	}
}

fn translate_def_type(inner_type: Type, array_size: ArraySize) -> Type {
	match array_size {
		ArraySize::None(_) => inner_type,
		ArraySize::Array(n, box ArraySize::None(ElemType::Byte)) =>
			Type::Array(((n + 3) / (size_of(Type::Word) as usize)) as u32, box inner_type),
		ArraySize::Array(n, box inner_size) =>
			Type::Array(n as u32, box translate_def_type(inner_type, inner_size)),
	}
}

fn translate_arg_type(inner_type: Type, array_size: ArrayDims) -> Type {
	match array_size {
		ArrayDims::None => inner_type,
		ArrayDims::Array(box inner_size) => Type::ArrayRef(
			box translate_arg_type(inner_type, inner_size)),
	}
}

fn is_ref_type(var_type: Type) -> bool {
	match var_type {
		Type::Array(_, _) => true,
		_ => false,
	}
}

fn size_of(var_type: Type) -> u32 {
	match var_type {
		Type::Word | Type::Chan => 4,
		Type::Array(n, box inner_type) => n * size_of(inner_type.clone())
			+ if is_ref_type(inner_type.clone()) { n * 4 } else { 0 },
		Type::ArrayRef(_) => 4,
	}
}

// BEGIN translate_2
fn translate_2(a_reg: Expr, b_reg: Expr, env: Environment,
	label_counter: &mut Counter) -> Result<(Asm, u32), AnyError> {
	let (a_asm, a_stack) =
		translate_expr(a_reg.clone(), env.clone(), label_counter)?;
	let (b_asm, b_stack) =
		translate_expr(b_reg.clone(), env.clone(), label_counter)?;
	// a_reg evaluation fits in register stack
	Ok(if a_stack <= 2 { (
		list![
			..b_asm,
			..a_asm,
		],
		max(b_stack, a_stack + 1)
	) }
	// b_reg evaluation fits in register stack
	else if b_stack <= 2 { (
		list![
			..a_asm,
			..b_asm,
			Op::Swap,
		],
		max(a_stack, b_stack + 1)
	) }
	// Spill a_reg to main stack
	else {
		let new_env = env.extend_var("<TEMP>".into(), Type::Word);
		let (b_asm, _) =
			translate_expr(b_reg.clone(), new_env.clone(), label_counter)?;
		(
			list![
				..a_asm,
				Op::AjW(-(size_of(Type::Word) as i32)),
				Op::StL(0),
				..b_asm,
				Op::LdL(0),
				Op::AjW(size_of(Type::Word) as i32),
			],
			max(a_stack, b_stack + 1)
		)
	})
}
// END translate_2

fn translate_3(a_reg: Expr, b_reg: Expr, c_reg: Expr, env: Environment,
	label_counter: &mut Counter) -> Result<(Asm, u32), AnyError> {
	let (a_asm, a_stack) = translate_expr(a_reg.clone(), env.clone(), label_counter)?;
	let (b_asm, b_stack) = translate_expr(b_reg.clone(), env.clone(), label_counter)?;
	let (ac_asm, ac_stack) =
		translate_2(a_reg.clone(), c_reg.clone(), env.clone(), label_counter)?;
	let (bc_asm, bc_stack) =
		translate_2(b_reg.clone(), c_reg.clone(), env.clone(), label_counter)?;
	// a_reg evaluation fits in register stack
	Ok(if a_stack <= 1 { (
		list![
			..bc_asm,
			..a_asm,
		],
		max(bc_stack, a_stack + 1)
	) }
	// b_reg evaluation fits in register stack
	else if b_stack <= 1 { (
		list![
			..ac_asm,
			..b_asm,
			Op::Swap,
		],
		max(ac_stack, b_stack + 1)
	) }
	// Spill a_reg to main stack
	else if bc_stack <= 3 || ac_stack > 3 {
		let new_env = env.extend_var("<TEMP>".into(), Type::Word);
		let (bc_asm, _) =
			translate_2(b_reg.clone(), c_reg.clone(), new_env.clone(), label_counter)?;
		(
			list![
				..a_asm,
				Op::AjW(-(size_of(Type::Word) as i32)),
				Op::StL(0),
				..bc_asm,
				Op::LdL(0),
				Op::AjW(size_of(Type::Word) as i32),
			],
			max(a_stack, bc_stack + 1)
		)
	}
	// Spill b_reg to main stack
	else {
		let new_env = env.extend_var("<TEMP>".into(), Type::Word);
		let (ac_asm, _) =
			translate_2(a_reg.clone(), c_reg.clone(), new_env.clone(), label_counter)?;
		(
			list![
				..b_asm,
				Op::AjW(-(size_of(Type::Word) as i32)),
				Op::StL(0),
				..ac_asm,
				Op::LdL(0),
				Op::Swap,
				Op::AjW(size_of(Type::Word) as i32),
			],
			max(b_stack, ac_stack + 1)
		)
	})
}
