use lalrpop_util::lalrpop_mod;
lalrpop_mod!(grammar);

pub mod ast;
use ast::Occam;

use super::lexer::Lexer;
use crate::AnyError;

pub fn parse(lexer: Lexer) -> Result<Occam, AnyError> {
	Ok(grammar::OccamParser::new().parse(lexer)?)
}
