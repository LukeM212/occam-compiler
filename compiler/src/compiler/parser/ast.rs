use crate::common::List;

#[derive(Debug, Clone)]
pub struct Occam {
	pub program: Proc,
}

// BEGIN process_ast
#[derive(Debug, Clone)]
pub enum Proc {
	Skip,
	Stop,
	Val(ValType, Name, ArraySize, Box<Proc>),
	Proc(Name, List<(ValType, Name, ArrayDims)>, Box<Proc>, Box<Proc>),
	Assign(Expr, Expr),
	Send(Expr, List<Expr>),
	Receive(Expr, List<Expr>),
	SendAlt(Expr, List<Expr>),
	ReceiveAlt(Expr, List<Expr>),
	Call(Expr, List<Expr>),
	If(List<(Expr, Proc)>),
	Par(List<Proc>),
	Seq(List<Proc>),
	Alt(List<(Option<Expr>, Option<(Expr, Name)>, Proc)>),
	While(Expr, Box<Proc>),
	ParLoop(Name, Expr, Expr, Box<Proc>),
	SeqLoop(Name, Expr, Expr, Box<Proc>),
	IfLoop(Name, Expr, Expr, Expr, Box<Proc>),
}
// END process_ast

pub type Name = String;

#[derive(Debug, Clone)]
pub enum ValType {
	Def(Expr),
	Var,
	Value,
	Chan,
}

#[derive(Debug, Copy, Clone)]
pub enum ElemType {
	Byte,
	Word,
}

#[derive(Debug, Clone)]
pub enum ArraySize {
	None(ElemType),
	Array(usize, Box<ArraySize>),
}

#[derive(Debug, Clone)]
pub enum ArrayDims {
	None,
	Array(Box<ArrayDims>),
}

#[derive(Debug, Clone)]
pub enum Expr {
	Const(i32),
	Name(Name),
	Table(ElemType, List<Expr>),
	MonOp(MonOp, Box<Expr>),
	BinOp(BinOp, Box<Expr>, Box<Expr>),
	Index(ElemType, Box<Expr>, Box<Expr>),
	Call(Box<Expr>, List<Expr>),
}

#[derive(Debug, Copy, Clone)]
pub enum MonOp {
	Neg,
	Not,
	Addr,
	Load,
}

#[derive(Debug, Copy, Clone)]
pub enum BinOp {
	Add,
	Sub,
	Mul,
	Div,
	Mod,
	After,
	LSL,
	LSR,
	
	And,
	Or,
	XOr,
	
	Eq,
	NEq,
	LT,
	GT,
	LEq,
	GEq,
}
