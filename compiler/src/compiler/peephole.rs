use super::instr_gen::{Asm, Op};
use crate::AnyError;
use crate::{list, listp};

// BEGIN peephole
pub fn peephole_optimise(mut asm: Asm) -> Result<Asm, AnyError> {
	let mut optimised = list![];
	Ok(loop {
		asm = match asm {
			listp![Op::AjW(a), Op::AjW(b), ..rest] => list![Op::AjW(a + b), ..rest],
			listp![Op::LdLP(a), Op::LdNL(b), ..rest] => list![Op::LdL(a + b), ..rest],
			listp![Op::LdLP(a), Op::LdNLP(b), ..rest] => list![Op::LdLP(a + b), ..rest],
			listp![Op::LdLP(a), Op::StNL(b), ..rest] => list![Op::StL(a + b), ..rest],
			listp![Op::LdC(c), Op::Add, ..rest] => list![Op::AddC(c), ..rest],
			listp![op, ..rest] => {
				optimised.push(op);
				rest
			},
			listp![] => break optimised.reverse(),
		};
	})
}
// END peephole