use crate::common::{List, MultiPeek};
use std::{str::Chars, fmt::{self, Debug, Display, Formatter}};
use crate::AnyError;

pub type Spanned<Tok, Loc, Error> = Result<(Loc, Tok, Loc), Error>;

pub fn lex(code: &str) -> Result<Lexer, AnyError> {
	Ok(Lexer::new(code))
}

#[derive(Debug, Clone)]
pub enum Tok {
	Int(i32),
	Str(String),
	Char(char),
	Name(String),
	
	Indent,
	SameIndent,
	Unindent,
	
	Skip,
	Stop,
	Def,
	Value,
	Var,
	Chan,
	Proc,
	If,
	Par,
	Seq,
	Alt,
	While,
	For,
	Byte,
	Table,
	After,
	And,
	Or,
	
	Colon,
	Semi,
	Assign,
	Eq,
	Bang,
	Ques,
	BangBang,
	QuesQues,
	LPar,
	RPar,
	LBra,
	RBra,
	Comma,
	Ampersand,
	Minus,
	FSlash,
	BSlash,
	Wedge,
	Vee,
	LShift,
	RShift,
	NEq,
	LT,
	GT,
	LTE,
	GTE,
	Plus,
	Times,
	XOr,
}

impl Display for Tok {
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		Debug::fmt(self, f)
	}
}

#[derive(Debug, Clone)]
pub enum LexicalError {
	UnknownSymbol(String),
}

impl Display for LexicalError {
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		match self {
			LexicalError::UnknownSymbol(s) => write!(f, "Unknown Symbol '{}'", s),
		}
	}
}

#[derive(Clone)]
pub struct Lexer<'input> {
	chars: MultiPeek<Chars<'input>, 2>,
	pos: usize,
	indent: usize,
	consumed: List<Spanned<Token, usize, LexicalError>>,
}

// Intermediate stage during tokenisation.
// Ideally these would be the tokens, but lalrpop doesn't support pattern
//   matching on tokens, so each keyword needs its own token type.
#[derive(Debug, Clone)]
enum Token {
	Word(String),
	Indent,
	SameIndent,
	Unindent,
}

impl<'input> Lexer<'input> {
	pub fn new(input: &'input str) -> Self {
		Lexer {
			chars: MultiPeek::new(input.chars()),
			pos: 0,
			indent: 0,
			consumed: List::Nil,
		}
	}
	
	fn tok(&mut self) -> List<Spanned<Token, usize, LexicalError>> {
		let start = self.pos;
		let tokens = loop {
			self.pos += 1;
			match self.chars.next() {
				None => break List::Nil, // End of file, so no more tokens
				
// BEGIN tokenise_indent
				// Match \n as newline character, ignore \r to support Windows
				Some('\n') => {
					// Check for new indentation level
					let mut spaces = 0;
					while self.chars.peek(0) == Some(&' ') {
						spaces += 1;
						self.pos += 1;
						self.chars.next();
					}
					
					// Don't insert indentation tokens for comments
					if self.chars.peek(0) == Some(&'-')
						&& self.chars.peek(1) == Some(&'-') {
						while self.chars.peek(0).is_some()
							&& self.chars.peek(0) != Some(&'\n') {
							self.pos += 1;
							self.chars.next();
						}
						continue;
					}
					
					assert_eq!(spaces % 2, 0);
					
					let old_indent = self.indent;
					self.indent = spaces / 2;
					let difference = self.indent as i32 - old_indent as i32;
					
					if difference > 0 {
						break List::copy(Token::Indent, difference as usize)
					} else if difference < 0 {
						break List::copy(Token::Unindent, (-difference) as usize)
					} else {
						break List::cons(Token::SameIndent, List::Nil)
					}
				},
// END tokenise_indent
				
				Some(c) if c.is_whitespace() => (), // Ignore whitespace
				
				Some('-') if self.chars.peek(0) == Some(&'-') => { // Comment
					while self.chars.peek(0).is_some()
						&& self.chars.peek(0) != Some(&'\n') {
						self.pos += 1;
						self.chars.next();
					}
				},
				
				Some('"') => { // String
					return List::cons(Err(LexicalError::UnknownSymbol(
						"Strings are not yet supported".to_string())), List::Nil);
				}
				
// BEGIN tokenise_symbol
				Some(c) if c.is_ascii_punctuation() => // Symbol
					break List::cons(Token::Word(c.to_string()), List::Nil),
// END tokenise_symbol
				
// BEGIN tokenise_other
				Some(c) if c.is_ascii_alphanumeric() => { // Other
					let mut word: String = c.to_string();
					while let Some(c) = self.chars.peek(0) {
						if *c == '.' || c.is_ascii_alphanumeric() {
							word.push(*c);
							self.pos += 1;
							self.chars.next();
						}
						else { break; }
					}
					break List::cons(Token::Word(word), List::Nil);
				},
// END tokenise_other
				
				Some(c) => return List::cons(Err(
					LexicalError::UnknownSymbol(c.to_string())), List::Nil),
			}
		};
		let end = self.pos;
		
		// The start and end indexes aren't exact for indentation, but it's close
		//   enough to be useful to the programmer
		tokens.map(|tok| Ok((start, tok, end))).collect()
	}
	
	fn take_part(&mut self) -> Option<Spanned<Token, usize, LexicalError>> {
		if self.consumed.is_empty() { self.consumed = self.tok(); }
		self.consumed.pop()
	}
	
	fn peek_part(&mut self) -> Option<&Spanned<Token, usize, LexicalError>> {
		if self.consumed.is_empty() { self.consumed = self.tok(); }
		self.consumed.head()
	}
}

impl<'input> Iterator for Lexer<'input> {
	type Item = Spanned<Tok, usize, LexicalError>;
	
	fn next(&mut self) -> Option<Self::Item> {
		self.take_part().map(|t| t.and_then(|(start, token, end)| {
			let tok = match token {
				Token::Word(w) => match w.as_ref() {
					"skip" => Tok::Skip,
					"stop" => Tok::Stop,
					"def" => Tok::Def,
					"value" => Tok::Value,
					"var" => Tok::Var,
					"chan" => Tok::Chan,
					"proc" => Tok::Proc,
					"if" => Tok::If,
					"par" => Tok::Par,
					"seq" => Tok::Seq,
					"alt" => Tok::Alt,
					"while" => Tok::While,
					"for" => Tok::For,
					"byte" => Tok::Byte,
					"table" => Tok::Table,
					"after" => Tok::After,
					"and" => Tok::And,
					"or" => Tok::Or,
					
// BEGIN token_symbol
					":" => {
						if let Some(Ok((_, Token::Word(c2), end))) = self.peek_part() {
							let end = *end;
							match c2.as_ref() {
								"=" => {
									self.take_part();
									return Ok((start, Tok::Assign, end))
								},
								_ => (),
							}
						}
						
						Tok::Colon
					},
// END token_symbol
					
					"<" => {
						if let Some(Ok((_, Token::Word(c2), end))) = self.peek_part() {
							let end = *end;
							match c2.as_ref() {
								"<" => {
									self.take_part();
									return Ok((start, Tok::LShift, end))
								},
								">" => {
									self.take_part();
									return Ok((start, Tok::NEq, end))
								},
								"=" => {
									self.take_part();
									return Ok((start, Tok::LTE, end))
								},
								_ => (),
							}
						}
						
						Tok::LT
					},
					
					">" => {
						if let Some(Ok((_, Token::Word(c2), end))) = self.peek_part() {
							let end = *end;
							match c2.as_ref() {
								">" => {
									self.take_part();
									return Ok((start, Tok::RShift, end))
								},
								"<" => {
									self.take_part();
									return Ok((start, Tok::XOr, end))
								},
								"=" => {
									self.take_part();
									return Ok((start, Tok::GTE, end))
								},
								_ => (),
							}
						}
						
						Tok::GT
					},
					
					"/" => {
						if let Some(Ok((_, Token::Word(c2), end))) = self.peek_part() {
							let end = *end;
							match c2.as_ref() {
								"\\" => {
									self.take_part();
									return Ok((start, Tok::Wedge, end))
								},
								_ => (),
							}
						}
						
						Tok::FSlash
					},
					
					"\\" => {
						if let Some(Ok((_, Token::Word(c2), end))) = self.peek_part() {
							let end = *end;
							match c2.as_ref() {
								"/" => {
									self.take_part();
									return Ok((start, Tok::Vee, end))
								},
								_ => (),
							}
						}
						
						Tok::BSlash
					},
					
					"!" => {
						if let Some(Ok((_, Token::Word(c2), end))) = self.peek_part() {
							let end = *end;
							match c2.as_ref() {
								"!" => {
									self.take_part();
									return Ok((start, Tok::BangBang, end))
								},
								_ => (),
							}
						}
						
						Tok::Bang
					},
					
					"?" => {
						if let Some(Ok((_, Token::Word(c2), end))) = self.peek_part() {
							let end = *end;
							match c2.as_ref() {
								"?" => {
									self.take_part();
									return Ok((start, Tok::QuesQues, end))
								},
								_ => (),
							}
						}
						
						Tok::Ques
					},
					
					";" => Tok::Semi,
					"=" => Tok::Eq,
					"(" => Tok::LPar,
					")" => Tok::RPar,
					"[" => Tok::LBra,
					"]" => Tok::RBra,
					"," => Tok::Comma,
					"&" => Tok::Ampersand,
					"-" => Tok::Minus,
					"+" => Tok::Plus,
					"*" => Tok::Times,
					
					"false" => Tok::Int(0),
					"true" => Tok::Int(-1),
					
// BEGIN token_special
					s if s.parse::<i32>().is_ok() => Tok::Int(s.parse::<i32>().unwrap()),
					
					s if s.chars().all(|c| c.is_ascii_alphabetic() || c == '.') =>
						Tok::Name(s.to_string()),
// END token_special
					
					s => return Err(LexicalError::UnknownSymbol(s.to_string())),
				},
				
				Token::Indent => Tok::Indent,
				Token::SameIndent => Tok::SameIndent,
				Token::Unindent => Tok::Unindent,
			};
			
			// Default to the same span, return directly if something else is needed
			Ok((start, tok, end))
		}))
	}
}

impl <'input> Debug for Lexer<'input> {
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		fmt.debug_list()
			.entries(self.clone().map(|x| x.map(|(_, symbol, _)| symbol)))
			.finish()
	}
}
