use std::collections::HashMap;
use super::instr_gen::{Asm, Op};
use crate::AnyError;
use crate::common::List;
use crate::instruction_set::Encoding;

// BEGIN assembler
pub fn assemble(asm: Asm) -> Result<Vec<u8>, AnyError> {
	let mut estimates = asm;
	let mut last_length = 0;
	loop {
		let mut label_pos = HashMap::new();
		
		let mut current_position = 0;
		for op in estimates.refs() {
			match op {
				Op::Label(label) => { label_pos.insert(label.0, current_position); },
				_ => { },
			}
			current_position += op.encode().len();
		}
		
		let mut current_position = 0;
		for op in estimates.refs_mut() {
			current_position += op.encode().len();
			match op {
				Op::J(label) | Op::CJ(label) | Op::StartP(label) | Op::LdPI(label) => {
					label.1 = label_pos.get(&label.0).unwrap() - current_position;
				},
				_ => { },
			}
		}
		
		if current_position == last_length { break; }
		else { last_length = current_position; }
	}
	
	let encoding: List<Encoding> = estimates.map(Op::encode).flatten().collect();
	
	Ok(encoding.map(|e| e.0).collect())
}
// END assembler
