use rustacuda::prelude::*;
use rustacuda::memory::DeviceBuffer;
use rustacuda::launch;
use std::ffi::CString;

const MEM_SIZE: usize = 1 << 24;
const CORE_COUNT: (usize, usize) = (2048, 32);

// BEGIN core_definition
#[repr(C)]
#[derive(Copy, Clone, DeviceCopy, Debug)]
struct Core {
	lptr: u32,
	wptr: u32,
}

impl Core {
	pub const DISABLED: Core = Core {
		lptr: u32::MAX,
		wptr: 0,
	};
}
// END core_definition

pub fn interpret(asm: &[u8], log: bool) {
// BEGIN select_gpu
	rustacuda::init(CudaFlags::empty()).unwrap();
	
	let device = Device::get_device(0).unwrap();
	
	let _context = Context::create_and_push(ContextFlags::MAP_HOST
		| ContextFlags::SCHED_AUTO, device);
// END select_gpu
	
// BEGIN compile_ptx
	let module_data = CString::new(include_str!("./run.ptx")).unwrap();
	let module = Module::load_from_string(&module_data).unwrap();
// END compile_ptx
	
// BEGIN allocate_memory
	let mut memory = box [0i32; MEM_SIZE >> 2];
	let mut cores = [Core::DISABLED; CORE_COUNT.1];
	
	cores[0] = Core { lptr: 0, wptr: MEM_SIZE as u32 };
	
	let mut asm_buffer = DeviceBuffer::from_slice(asm).unwrap();
	let mut memory_buffer = DeviceBuffer::from_slice(&memory[..]).unwrap();
	let mut cores_buffer = DeviceBuffer::from_slice(&cores[..]).unwrap();
// END allocate_memory
	
// BEGIN run
	let stream = Stream::new(StreamFlags::NON_BLOCKING, None).unwrap();
	
	unsafe {
		launch!(module.run<<<CORE_COUNT.0 as u32, CORE_COUNT.1 as u32, 0, stream>>>(
			asm_buffer.as_device_ptr(),
			memory_buffer.as_device_ptr(),
			cores_buffer.as_device_ptr()
		)).unwrap();
	}
	
	stream.synchronize().unwrap();
	
	memory_buffer.copy_to(&mut memory[..]).unwrap();
	cores_buffer.copy_to(&mut cores[..]).unwrap();
// END run
	
	if log {
		println!("Memory:");
		
		for i in ((MEM_SIZE >> 2) - 128)..(MEM_SIZE >> 2) {
			println!("{}: {:?}", i << 2, memory[i]);
		}
		
		println!("Cores:");
		
		for i in 0..CORE_COUNT.1 {
			println!("{} {:?}", i, cores[i]);
		}
	}
}
