use std::sync::{atomic::{AtomicI32, Ordering}, Mutex};
use std::convert::TryFrom;
use std::mem;
use threadpool::ThreadPool;
use higher_order_functions::Init;
use crate::instruction_set::{Encoding, OpCode, Operation};

const MEM_SIZE: usize = 1 << 24;

pub fn interpret(asm: &[u8], log: bool) {
	let mem = box <[AtomicI32; MEM_SIZE >> 2]>::init(|_| AtomicI32::new(0));
	let pool = Mutex::new(ThreadPool::new(32));
	let thread_counter = AtomicI32::new(0);
	unsafe {
		// Use unsafe to assert that these references will live long enough
		let asm: &'static [u8] = mem::transmute(asm);
		let mem: &'static [AtomicI32] = mem::transmute(&mem[..]);
		let thread_counter: &'static AtomicI32 = mem::transmute(&thread_counter);
		let pool_ref: &'static Mutex<ThreadPool> = mem::transmute(&pool);
		pool.lock().unwrap().execute(move ||
			thread(pool_ref, asm, mem, 0, MEM_SIZE, thread_counter, log));
		// Use unsafe to call pool.join() without locking on the Mutex.
		// ThreadPool is represented as ThreadPool { StartTaskChannel, Arc<InternalState> }
		// The latter is safe to share between threads, but the former is not
		// However, .join() does not need to use the StartTaskChannel, so we can
		//   safely access it without a Mutex
		// There is currently a GitHub issue to fix this problem with the ThreadPool
		//   library by using a Mutex<StartTaskChannel> internally, but it has not
		//   yet been implemented.
		let pool_guard = pool.lock().unwrap();
		let unsafe_pointer: *const ThreadPool = &*pool_guard;
		drop(pool_guard);
		unsafe_pointer.as_ref().unwrap().join();
	}
	if log {
		for i in ((MEM_SIZE >> 2) - 128)..(MEM_SIZE >> 2) {
			println!("{}: {:?}", i << 2, mem[i]);
		}
	}
}

// BEGIN thread
fn thread(pool: &'static Mutex<ThreadPool>, asm: &'static [u8],
	mem: &'static [AtomicI32], mut lptr: usize, mut wptr: usize,
	thread_counter: &'static AtomicI32, log: bool,
) {
	let thread_id = thread_counter.fetch_add(1, Ordering::Relaxed);
	if log { println!("[{}] Thread started!", thread_id); }
	
	let mut oreg: i32 = 0;
	let mut areg: i32 = 0;
	let mut breg: i32 = 0;
	let mut creg: i32 = 0;
	
	loop {
		let encoding = Encoding::from(asm[lptr]);
		
		oreg |= encoding.operand() as i32;
		
		if log {
			println!("[{}] {:X?} ({}) {} {:?} {} ({}, {}, {}, {})", thread_id, lptr,
			lptr, wptr, encoding.opcode(), encoding.operand(), oreg, areg, breg, creg);
		}
		
		lptr += 1;
		
		match encoding.opcode() {
// END thread
// BEGIN example_1
			OpCode::J => {
				lptr = (lptr as i32 + oreg) as usize;
				oreg = 0;
			},
// END example_1
			OpCode::LdLP => {
				creg = breg;
				breg = areg;
				areg = wptr as i32 + oreg;
				oreg = 0;
			},
			OpCode::Pfix => {
				oreg = oreg << 4;
			},
			OpCode::LdNL => {
				areg = mem[(areg + oreg) as usize >> 2].load(Ordering::Relaxed);
				oreg = 0;
			},
			OpCode::LdC => {
				creg = breg;
				breg = areg;
				areg = oreg;
				oreg = 0;
			},
			OpCode::Nfix => {
				oreg = !oreg << 4;
			},
// BEGIN example_2
			OpCode::LdL => {
				creg = breg;
				breg = areg;
				areg = mem[(wptr as i32 + oreg) as usize >> 2].load(Ordering::Relaxed);
				oreg = 0;
			},
// END example_2
			OpCode::AdC => {
				areg += oreg;
				oreg = 0;
			},
			OpCode::CJ => {
				if areg == 0 { lptr = (lptr as i32 + oreg) as usize; }
				areg = breg;
				breg = creg;
				oreg = 0;
			},
			OpCode::AjW => {
				wptr = (wptr as i32 + oreg) as usize;
				oreg = 0;
			},
			OpCode::EqC => {
				areg = if areg == oreg { -1 } else { 0 };
				oreg = 0;
			},
			OpCode::StL => {
				mem[(wptr as i32 + oreg) as usize >> 2].store(areg, Ordering::Relaxed);
				areg = breg;
				breg = creg;
				oreg = 0;
			},
			OpCode::StNL => {
				mem[(areg + oreg) as usize >> 2].store(breg, Ordering::Relaxed);
				areg = creg;
				oreg = 0;
			},
// BEGIN operate
			OpCode::Opr => {
				if log {
					println!("[{}] ({:?})", thread_id, Operation::try_from(oreg).unwrap());
				}
				
				match Operation::try_from(oreg).unwrap() {
// END operate
					Operation::Rev => {
						let tmp = areg;
						areg = breg;
						breg = tmp;
					},
					Operation::Copy => {
						creg = breg;
						breg = areg;
					},
					Operation::EndP => {
						let new_wptr = areg as usize;
						let old = mem[new_wptr >> 2].fetch_sub(1, Ordering::Relaxed);
						if old == 0 {
							let new_lptr =
								mem[(new_wptr >> 2) - 1].load(Ordering::Relaxed) as usize;
							pool.lock().unwrap().execute(move ||
								thread(pool, asm, mem, new_lptr, new_wptr, thread_counter, log));
						}
						return;
					},
					Operation::Sub => {
						areg = breg - areg;
						breg = creg;
					},
					Operation::Add => {
						areg = breg + areg;
						breg = creg;
					},
// BEGIN example_3
					Operation::In => {
						let length = areg as usize;
						let chan = breg as usize;
						let ptr = creg as usize;
						mem[(wptr >> 2) - 2].store(ptr as i32, Ordering::Relaxed);
						mem[(wptr >> 2) - 1].store(lptr as i32, Ordering::Relaxed);
						let other = mem[chan >> 2].fetch_update(Ordering::Relaxed,
							Ordering::Relaxed, |value| {
							if value == -1 { Some(wptr as i32) }
							else { Some(-1) }
						}).unwrap();
						if other != -1 {
							let other = other as usize;
							let other_ptr =
								mem[(other >> 2) - 2].load(Ordering::Relaxed) as usize;
							let other_lptr =
								mem[(other >> 2) - 1].load(Ordering::Relaxed) as usize;
							for i in 0..(length >> 2) {
								let value = mem[(other_ptr >> 2) + i].load(Ordering::Relaxed);
								if log {
									println!("{} -{}-> {}: {}", other_ptr, chan, ptr, value);
								}
								mem[(ptr >> 2) + i].store(value, Ordering::Relaxed);
							}
							pool.lock().unwrap().execute(move ||
								thread(pool, asm, mem, other_lptr, other, thread_counter, log));
						} else {
							return;
						}
					},
// END example_3
					Operation::Mul => {
						areg = breg * areg;
						breg = creg;
					},
					Operation::GT => {
						areg = if breg > areg { -1 } else { 0 };
						breg = creg;
					},
					Operation::Out => {
						let length = areg as usize;
						let chan = breg as usize;
						let ptr = creg as usize;
						mem[(wptr >> 2) - 2].store(ptr as i32, Ordering::Relaxed);
						mem[(wptr >> 2) - 1].store(lptr as i32, Ordering::Relaxed);
						let other = mem[chan >> 2].fetch_update(
							Ordering::Relaxed, Ordering::Relaxed, |value| {
							if value == -1 { Some(wptr as i32) }
							else { Some(-1) }
						}).unwrap();
						if other != -1 {
							let other = other as usize;
							let other_ptr =
								mem[(other >> 2) - 2].load(Ordering::Relaxed) as usize;
							let other_lptr =
								mem[(other >> 2) - 1].load(Ordering::Relaxed) as usize;
							for i in 0..(length >> 2) {
								let value = mem[(ptr >> 2) + i].load(Ordering::Relaxed);
								if log { println!("{} -{}-> {}: {}", ptr, chan, other_ptr, value); }
								mem[(other_ptr >> 2) + i].store(value, Ordering::Relaxed);
							}
							pool.lock().unwrap().execute(move ||
								thread(pool, asm, mem, other_lptr, other, thread_counter, log));
						} else {
							return;
						}
					},
// BEGIN example_4
					Operation::StartP => {
						let new_lptr = lptr + (areg as usize);
						let new_wptr = breg as usize;
						pool.lock().unwrap().execute(move || thread(pool, asm, mem, new_lptr,
							new_wptr, thread_counter, log));
						areg = creg;
					},
// END example_4
					Operation::StopP => {
						mem[(wptr >> 2) - 1].store(lptr as i32, Ordering::Relaxed);
						return;
					},
					Operation::LdPI => {
						areg += lptr as i32;
					},
					Operation::Rem => {
						areg = breg % areg;
						breg = creg;
					},
					Operation::Ret => {
						lptr = mem[wptr >> 2].load(Ordering::Relaxed) as usize;
						wptr += 4;
					},
					Operation::Div => {
						areg = breg / areg;
						breg = creg;
					},
					Operation::Not => {
						areg = !areg;
					},
					Operation::XOr => {
						areg = breg ^ areg;
						breg = creg;
					},
					Operation::RunP => {
						let new_wptr = areg as usize;
						let new_lptr =
							mem[(new_wptr >> 2) - 1].load(Ordering::Relaxed) as usize;
						pool.lock().unwrap().execute(move ||
							thread(pool, asm, mem, new_lptr, new_wptr, thread_counter, log));
						areg = breg;
						breg = creg;
					},
					Operation::ShR => {
						areg = breg >> areg;
						breg = creg;
					},
					Operation::ShL => {
						areg = breg << areg;
						breg = creg;
					},
					Operation::And => {
						areg = breg & areg;
						breg = creg;
					},
					Operation::Or => {
						areg = breg | areg;
						breg = creg;
					},
					Operation::CAS => {
						areg = mem[areg as usize >> 2].compare_exchange(breg, creg,
							Ordering::Relaxed, Ordering::Relaxed).unwrap_or_else(|x| x);
						oreg = 0;
					},
				}
				oreg = 0;
			},
		}
	}
}
