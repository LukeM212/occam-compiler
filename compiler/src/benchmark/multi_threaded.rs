use std::thread;
use std::sync::mpsc::{self, SyncSender, Receiver};
use threadpool::ThreadPool;

#[inline(never)]
pub fn multi_threaded_benchmark() -> u32 {
	let pool = ThreadPool::new(32);
	let channels: Vec<Receiver<u32>> = (0..40_000).map(|n| {
		let (tx, rx) = mpsc::sync_channel(0);
		pool.execute(move || {
			let mut my_result = n;
			for i in 0..1_000_000 {
				my_result *= i;
				my_result += i;
			}
			tx.send(my_result).unwrap();
		});
		rx
	}).collect();
	
	let mut result = 0;
	for c in channels {
		result += c.recv().unwrap();
	}
	result
}
