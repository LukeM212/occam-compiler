use std::time::Instant;

use crate::{compiler, interpreter_cpu, interpreter_gpu};

mod single_threaded;
mod multi_threaded;
mod all_coprime;

pub fn run_benchmarks() {
	let start = Instant::now();
	let result = single_threaded::single_threaded_benchmark();
	println!("Single Threaded Native CPU: {:?}", start.elapsed());
	println!("{}", result);
	
	let occam = include_str!("single_threaded.occam").into();
	let transputer_code = compiler::compile(occam).unwrap();
	
	let start = Instant::now();
	interpreter_cpu::interpret(&transputer_code, false);
	println!("Single Threaded CPU Interpreter: {:?}", start.elapsed());
	
	let start = Instant::now();
	interpreter_gpu::interpret(&transputer_code, false);
	println!("Single Threaded GPU Interpreter: {:?}", start.elapsed());
	
	
	let start = Instant::now();
	let result = multi_threaded::multi_threaded_benchmark();
	println!("Multi Threaded Native CPU: {:?}", start.elapsed());
	println!("{}", result);
	
	let occam = include_str!("multi_threaded.occam").into();
	let transputer_code = compiler::compile(occam).unwrap();
	
	let start = Instant::now();
	interpreter_cpu::interpret(&transputer_code, false);
	println!("Multi Threaded CPU Interpreter: {:?}", start.elapsed());
	
	let start = Instant::now();
	interpreter_gpu::interpret(&transputer_code, false);
	println!("Multi Threaded GPU Interpreter: {:?}", start.elapsed());
	
	
	let start = Instant::now();
	let result = all_coprime::coprime_benchmark();
	println!("Coprime Native CPU: {:?}", start.elapsed());
	println!("{}", result);
	
	let occam = include_str!("all_coprime.occam").into();
	let transputer_code = compiler::compile(occam).unwrap();
	
	let start = Instant::now();
	interpreter_cpu::interpret(&transputer_code, false);
	println!("Coprime CPU Interpreter: {:?}", start.elapsed());
	
	let start = Instant::now();
	interpreter_gpu::interpret(&transputer_code, false);
	println!("Coprime GPU Interpreter: {:?}", start.elapsed());
}
