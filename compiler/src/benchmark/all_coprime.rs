use std::thread;
use std::sync::mpsc::{self, SyncSender, Receiver};
use threadpool::ThreadPool;

#[inline(never)]
pub fn coprime_benchmark() -> u32 {
	let mut values = Box::new([0; 20000]);
	let mut x = 0;
	for i in 0..20000 {
		x *= i as i32;
		x += i as i32;
		if x < 0 {
			x = -x;
		}
		values[i] = x;
	}
	let values: &'static [i32; 20000] = Box::leak(values);
	
	let pool = ThreadPool::new(32);
	let channels: Vec<Receiver<u32>> = (0..20000).map(|i| {
		let (tx, rx) = mpsc::sync_channel(0);
		let values: &'static [i32; 20000] = <&[i32; 20000] as Clone>::clone(&&*values);
		pool.execute(move || {
			let mut my_count = 0;
			let v = values[i];
			for j in 0..20000 {
				let mut a = v;
				let mut b = values[j];
				while b != 0 {
					let t = b;
					b = a % b;
					a = t;
				}
				if a == 1 {
					my_count += 1
				}
			}
			tx.send(my_count).unwrap();
		});
		rx
	}).collect();
	
	let mut count = 0;
	for c in channels {
		count += c.recv().unwrap();
	}
	count
}
