#[inline(never)]
pub fn single_threaded_benchmark() -> u32 {
	let mut result = 0;
	for i in 0..10_000_000 {
		result *= i;
		result += i;
	}
	result
}
