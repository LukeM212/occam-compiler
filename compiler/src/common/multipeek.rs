use super::CircularQueue;

use std::fmt::{self, Debug, Formatter};

#[derive(Clone)]
pub struct MultiPeek<I: Iterator, const N: usize> {
	queue: CircularQueue<I::Item, N>,
	inner: I,
}

impl<I: Iterator, const N: usize> Iterator for MultiPeek<I, N> {
	type Item = I::Item;
	
	fn next(&mut self) -> Option<I::Item> {
		self.queue.next().or_else(|| self.inner.next())
	}
}


impl<I: Iterator, const N: usize> MultiPeek<I, N> {
	pub fn new(inner: I) -> Self {
		Self {
			queue: CircularQueue::new(),
			inner: inner,
		}
	}
	
	pub fn peek(&mut self, i: usize) -> Option<&I::Item> {
		assert!(i < N);
		while self.queue.len() <= i {
			match self.inner.next() {
				None => return None,
				Some(x) => self.queue.push(x).unwrap(),
			}
		}
		Some(&self.queue[i])
	}
}

impl<I: Iterator + Clone, const N: usize> Debug for MultiPeek<I, N> where I::Item: Clone + Debug {
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		fmt.debug_list()
			.entries(self.queue.clone())
			.entries(self.inner.clone())
			.finish()
	}
}
