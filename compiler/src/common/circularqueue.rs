use std::{
	mem::MaybeUninit,
	ops::{Index, IndexMut},
	fmt::{self, Debug, Formatter},
};

pub struct CircularQueue<T, const N: usize> {
	contents: [MaybeUninit<T>; N],
	start: usize,
	count: usize,
}

impl<T, const N: usize> CircularQueue<T, N> {
	pub fn new() -> Self {
		Self {
			contents: MaybeUninit::uninit_array(),
			start: 0,
			count: 0,
		}
	}
	
	pub fn push(&mut self, value: T) -> Result<(), &'static str> {
		if self.count >= N { Err("Out of capacity") }
		else {
			let next_i = self.count;
			self.contents[self.get_index(next_i)] = MaybeUninit::new(value);
			self.count += 1;
			Ok(())
		}
	}
	
	pub fn len(&self) -> usize {
		self.count
	}
	
	fn get_index(&self, i: usize) -> usize {
		let j = self.start + i;
		if j >= N { j - N } else { j }
	}
}

impl<T, const N: usize> Index<usize> for CircularQueue<T, N> {
	type Output = T;
	
	fn index(&self, i: usize) -> &T {
		assert!(i < self.count);
		unsafe { self.contents[self.get_index(i)].assume_init_ref() }
	}
}

impl<T, const N: usize> IndexMut<usize> for CircularQueue<T, N> {
	fn index_mut(&mut self, i: usize) -> &mut T {
		assert!(i < self.count);
		unsafe { self.contents[self.get_index(i)].assume_init_mut() }
	}
}

impl<T, const N: usize> Iterator for CircularQueue<T, N> {
	type Item = T;
	
	fn next(&mut self) -> Option<Self::Item> {
		if self.count == 0 { None }
		else {
			let result = unsafe { self.contents[self.start].assume_init_read() };
			self.start = self.get_index(1);
			self.count -= 1;
			Some(result)
		}
	}
}

impl<T, const N: usize> Drop for CircularQueue<T, N> {
	fn drop(&mut self) {
		// Need to manually drop initialised segment of MaybeUninit array
		for i in 0..self.count {
			unsafe { self.contents[self.get_index(i)].assume_init_read(); }
		}
	}
}

impl<T: Debug, const N: usize> Debug for CircularQueue<T, N> {
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		let mut list = fmt.debug_list();
		for i in 0..self.count {
			list.entry(&self[i]);
		}
		list.finish()
	}
}

impl<T: Clone, const N: usize> Clone for CircularQueue<T, N> {
	fn clone(&self) -> Self {
		let mut new_contents = MaybeUninit::uninit_array();
		
		for i in 0..self.count {
			new_contents[i] = MaybeUninit::new(self[i].clone());
		}
		
		CircularQueue {
			contents: new_contents,
			start: 0,
			count: self.count,
		}
	}
}
