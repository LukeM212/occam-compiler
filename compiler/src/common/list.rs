use std::iter::FromIterator;

use replace_with::replace_with_or_abort;

#[derive(Debug, Clone)]
pub enum List<T> {
	Nil,
	Cons(Box<(T, List<T>)>),
}

impl<T> List<T> {
	pub fn cons(x: T, xs: List<T>) -> List<T> {
		List::Cons(box (x, xs))
	}
	
	pub fn copy(x: T, n: usize) -> List<T> where T: Clone {
		let mut result = List::Nil;
		for _ in 0..n {
			result = List::cons(x.clone(), result);
		}
		result
	}
	
	pub fn is_empty(&self) -> bool {
		match self {
			List::Nil => true,
			List::Cons(_) => false,
		}
	}
	
	pub fn non_empty(&self) -> bool {
		!self.is_empty()
	}
	
	pub fn len(&self) -> i32 {
		let mut length = 0;
		let mut rest = self;
		while let List::Cons(box (_, ref xs)) = rest {
			rest = xs;
			length += 1;
		}
		length
	}
	
	pub fn head(&self) -> Option<&T> {
		match self {
			List::Nil => None,
			List::Cons(box (ref x, _)) => Some(x),
		}
	}
	
	pub fn tail(&self) -> Option<&List<T>> {
		match self {
			List::Nil => None,
			List::Cons(box (_, ref xs)) => Some(xs),
		}
	}
	
	pub fn head_mut(&mut self) -> Option<&mut T> {
		match self {
			List::Nil => None,
			List::Cons(box (ref mut x, _)) => Some(x),
		}
	}
	
	pub fn tail_mut(&mut self) -> Option<&mut List<T>> {
		match self {
			List::Nil => None,
			List::Cons(box (_, ref mut xs)) => Some(xs),
		}
	}
	
	pub fn pop(&mut self) -> Option<T> {
		let mut result = None;
		replace_with_or_abort(self, |s| match s {
			List::Nil => { result = None; List::Nil },
			List::Cons(box (x, xs)) => { result = Some(x); xs },
		});
		result
	}
	
	pub fn push(&mut self, x: T) {
		replace_with_or_abort(self, |s| List::cons(x, s));
	}
	
	pub fn reverse(self) -> List<T> {
		let mut result = List::Nil;
		let mut rest = self;
		while let List::Cons(box (x, xs)) = rest {
			result = List::cons(x, result);
			rest = xs;
		}
		result
	}
	
	pub fn concat(mut self, other: List<T>) -> List<T> {
		let mut rest = &mut self;
		while let List::Cons(box (_, ref mut xs)) = rest {
			rest = xs;
		}
		*rest = other;
		self
	}
	
	pub fn map<U, F: FnMut(T) -> U>(&self, mut f: F) -> List<U> where T: Clone {
		match self {
			List::Nil => List::Nil,
			List::Cons(box (x, xs)) => List::cons(f(x.clone()), xs.map(f)),
		}
	}
	
	pub fn refs<'a>(&'a self) -> List<&'a T> {
		match self {
			List::Nil => List::Nil,
			List::Cons(box (x, xs)) => List::cons(x, xs.refs()),
		}
	}
	
	pub fn refs_mut<'a>(&'a mut self) -> List<&'a mut T> {
		match self {
			List::Nil => List::Nil,
			List::Cons(box (x, xs)) => List::cons(x, xs.refs_mut()),
		}
	}
}

impl<T> List<List<T>> {
	pub fn flatten(self) -> List<T> {
		let mut start = List::Nil;
		let mut end = &mut start;
		
		for inner in self {
			for item in inner {
				*end = List::cons(item, List::Nil);
				end = end.tail_mut().unwrap();
			}
		}
		
		start
	}
}

impl<T> Iterator for List<T> {
	type Item = T;
	
	fn next(&mut self) -> Option<T> {
		self.pop()
	}
}

impl<T> FromIterator<T> for List<T> {
	fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
		let mut start = List::Nil;
		let mut end = &mut start;
		
		for x in iter {
			*end = List::cons(x, List::Nil);
			end = end.tail_mut().unwrap();
		}
		
		start
	}
}

#[macro_export]
macro_rules! list {
	[] => { $crate::common::List::Nil };
	[..$front:expr,$($rest:tt)*] => { ($front).concat(list![$($rest)*]) };
	[$front:expr,$($rest:tt)*] => { $crate::common::List::Cons(box ($front, list![$($rest)*])) };
	[..$front:expr] => { $front };
	[$front:expr] => { List::Cons(box ($front, $crate::common::List::Nil)) };
}

#[macro_export]
macro_rules! listp {
	[] => { $crate::common::List::Nil };
	[..$front:pat$(,)?] => { $front };
	[$front:pat,$($rest:tt)*] => { $crate::common::List::Cons(box ($front, listp![$($rest)*])) };
	[$front:pat] => { $crate::common::List::Cons(box ($front, $crate::common::List::Nil)) };
}
