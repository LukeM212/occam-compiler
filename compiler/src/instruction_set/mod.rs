use std::convert::TryFrom;
use std::fmt::{Formatter, Debug, self};
use num_enum::{TryFromPrimitive, IntoPrimitive};
use crate::common::List;

#[derive(Copy, Clone)]
pub struct Encoding(pub u8);

impl Encoding {
	pub fn new(opcode: OpCode, operand: u8) -> Encoding {
		Encoding(opcode.into(): u8 | operand)
	}
	
	pub fn opcode(self) -> OpCode {
		OpCode::try_from(self.0 & 0xF0).unwrap()
	}
	
	pub fn operand(self) -> u8 {
		self.0 & 0x0F
	}
}

impl From<u8> for Encoding {
	fn from(byte: u8) -> Encoding {
		Encoding(byte)
	}
}

impl Debug for Encoding {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{:?} {}", self.opcode(), self.operand())
	}
}

#[derive(Copy, Clone, PartialEq, Eq, Debug, TryFromPrimitive, IntoPrimitive)]
#[repr(u8)]
pub enum OpCode {
	J     = 0x10,
	LdLP  = 0x20,
	Pfix  = 0x30,
	LdNL  = 0x40,
	LdC   = 0x50,
	Nfix  = 0x60,
	LdL   = 0x70,
	AdC   = 0x80,
	CJ    = 0x90,
	AjW   = 0xA0,
	EqC   = 0xB0,
	StL   = 0xC0,
	StNL  = 0xD0,
	Opr   = 0xE0,
}

// BEGIN opcode
impl OpCode {
	pub fn encode(self, operand: i32) -> List<Encoding> {
		let last = Encoding::new(self, (operand & 0xF) as u8);
		let rest =
			if operand < 0 { OpCode::Nfix.encode(!operand >> 4) }
			else if operand >= 16 { OpCode::Pfix.encode(operand >> 4) }
			else { List::Nil };
		List::cons(last, rest.reverse()).reverse()
	}
}
// END opcode

#[derive(Copy, Clone, PartialEq, Eq, Debug, TryFromPrimitive, IntoPrimitive)]
#[repr(i32)]
pub enum Operation {
	Rev  = 0x0,
	Copy = 0x1,
	Add  = 0x2,
	Sub  = 0x3,
	Mul  = 0x4,
	Div  = 0x5,
	Rem  = 0x6,
	Ret  = 0x7,
	Not  = 0x8,
	And  = 0x9,
	Or   = 0xA,
	XOr  = 0xB,
	ShL  = 0xC,
	ShR  = 0xD,
	StartP = 0xE,
	RunP = 0xF,
	StopP = 0x10,
	EndP = 0x11,
	In   = 0x12,
	Out  = 0x13,
	GT   = 0x14,
	LdPI = 0x15,
	CAS =  0x16,
}

impl Operation {
	pub fn encode(self) -> List<Encoding> {
		OpCode::Opr.encode(self.into())
	}
}
