#![feature(box_syntax)]
#![feature(box_patterns)]
#![feature(const_generics)]
#![feature(maybe_uninit_extra)]
#![feature(maybe_uninit_ref)]
#![feature(maybe_uninit_uninit_array)]
#![feature(type_ascription)]

#[macro_use]
extern crate rustacuda;

mod common;
mod instruction_set;
mod compiler;
mod interpreter_cpu;
mod interpreter_gpu;
mod benchmark;

use std::error::Error;

type AnyError = Box<dyn Error>;

use simple_error::bail;

fn run() -> Result<(), AnyError> {
	let args: Vec<String> = std::env::args().collect();
	
	if args[1] == "--bench" {
		benchmark::run_benchmarks();
		return Ok(());
	}
	
	if args.len() < 3 { bail!("Usage: occam INFILE OUTFILE [--run (cpu/gpu)]"); }
	
	let in_file = &args[1];
	let out_file = &args[2];
	let run = args.get(3);
	
	let occam = std::fs::read_to_string(in_file)?;
	
	let transputer_code = compiler::compile(occam)?;
	
	std::fs::write(out_file, transputer_code.clone())?;
	
	if run == Some(&"--run".into()) {
		match args.get(4).map(String::as_str) {
			Some("cpu") => interpreter_cpu::interpret(&transputer_code, true),
			
			Some("gpu") => interpreter_gpu::interpret(&transputer_code, true),
			
			_ => bail!("Unknown interpreter type"),
		}
	}
	
	Ok(())
}

fn main() {
	std::process::exit(match run() {
		Ok(_) => 0,
		Err(err) => { eprintln!("Error: {}", err); 1 },
	});
}
